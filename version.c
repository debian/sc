/*
 * CODE REVISION NUMBER:
 *
 * The part after the first colon, except the last char, appears on the screen.
 * The part after the first underscore is the subrevision.
 * A second underscore marks a development version.
 */

extern char *rev;
char *rev = "$Revision: 7.16_1.1.2 $";
